﻿using UnityEngine;
using System.Collections;

public enum GameState
{
    NONE,
    SPLASH,
    MAIN_MENU,
    SETTINGS,
    CREDITS,
    LEVEL_SELECT,
    INGAME,
    LEVEL_FINISHED,
    ENDGAME,
}
