﻿using UnityEngine;
using System.Collections;

public enum EntityType 
{
    DEFAULT,
    PLAYER_ONE,
    PLAYER_TWO,
    MONSTER_AUTO
}
