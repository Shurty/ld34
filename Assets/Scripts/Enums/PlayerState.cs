﻿using UnityEngine;
using System.Collections;

public enum PlayerState
{
    SPAWNING,
    ALIVE,
    KILLED,
    DEAD
}
