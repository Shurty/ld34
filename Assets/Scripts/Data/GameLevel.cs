﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class GameLevel
{
    public string name;
    public string sceneId;
    public bool locked = true;
    public bool hasCinematic;
    public string cinematicName;
    public float cinematicTime;
    public int starsGotten = 0;

    public float bronzeTime = 30.0f;
    public float silverTime = 20.0f;
    public float goldTime = 10.0f;
}
