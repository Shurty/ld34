﻿using UnityEngine;
using System.Collections;

public class ActionInterruptor : MonoBehaviour {

    public GameObject renderUnder;
    public bool pressed;
    public GameObject targetObject;
    public ActionResponseType responseType;

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (pressed == true)
            return;
        if (collider.gameObject.GetComponentInChildren<BallsCollisionDetection>())
        {
            pressed = true;
            renderUnder.SetActive(false);
            if (targetObject != null)
            {
                targetObject.SendMessage("OnActionEventTriggered", responseType, SendMessageOptions.DontRequireReceiver);
                GameController.instance.sounds.PlayInterPressSound();
            }
        }
    }
}
