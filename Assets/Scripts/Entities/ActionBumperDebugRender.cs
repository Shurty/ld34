﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
[RequireComponent(typeof(ActionBumper))]
public class ActionBumperDebugRender : MonoBehaviour
{
    private ActionBumper ent = null;

	// Use this for initialization
	void Start () 
    {
        ent = gameObject.GetComponent<ActionBumper>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        Vector3 pos = transform.position;
        if (ent == null)
        {
            ent = gameObject.GetComponent<ActionBumper>();
        }
 
        DebugUtils.DrawRect(
            pos,
            pos + new Vector3(ent.direction.x, ent.direction.y, 0),
            pos,
            pos + new Vector3(ent.direction.x, ent.direction.y, 0),
            new Color(1, 0, 0, 1)
        );
	}
}
