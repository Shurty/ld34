﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class Rotator : MonoBehaviour 
{

    public float rotationSpeed;
    public Rigidbody2D body;

	// Use this for initialization
	void Start () 
    {
        body = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        body.angularVelocity = rotationSpeed;
	}
}
