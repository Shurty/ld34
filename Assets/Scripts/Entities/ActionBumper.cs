﻿using UnityEngine;
using System.Collections;

public class ActionBumper : MonoBehaviour
{

    public Vector3 direction;
    public float force;
    public float reactionDelay = 0.0f;

    public bool additive;
    public bool reflect;
    public bool accelerative;

    public AudioSource audioSrc;

    // Use this for initialization
	void Start () 
    {
        audioSrc = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (reactionDelay > 0.0f)
            reactionDelay -= Time.deltaTime;
	}

    public IEnumerator AccelerateCollider(Rigidbody2D body, Vector3 force, int times)
    {
        body.isKinematic = true;
        yield return true;
        body.isKinematic = false;

        while (times > 0)
        {
            body.AddForce(force);
            --times;
            yield return new WaitForSeconds(0.05f);
        }
        yield return true;
    }

    void OnTriggerEnter2D(Collider2D collision) 
    {
        if (reactionDelay > 0.0f)
            return;

        if (collision.GetComponentInParent<PlayerCharacter>() == null)
            return;

        Debug.Log("Collision");
        audioSrc.Play();
        Vector3 vector1 = new Vector3(0, 0, 0);
        Vector3 vector2;
        Rigidbody2D body = collision.GetComponentInParent<Rigidbody2D>();
        float targetForce = force;

        reactionDelay = 0.5f;
        vector2 = body.velocity;
        vector2.Normalize();

        if (reflect)
        {
            vector1 = Vector3.Reflect(vector2, direction);
        }
        else
        {
            vector1 = direction;
        }
        if (additive)
        {
            targetForce = targetForce * body.velocity.magnitude;
        }
        vector1.Normalize();

        if (accelerative)
        {
            StartCoroutine(AccelerateCollider(body, vector1 * targetForce * 0.1f, 10));
        }
        else
        {
            StartCoroutine(AccelerateCollider(body, vector1 * targetForce, 1));
        }

    }
}
