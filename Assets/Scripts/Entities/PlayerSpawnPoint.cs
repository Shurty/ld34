﻿using UnityEngine;
using System.Collections;

public class PlayerSpawnPoint : MonoBehaviour 
{
    public EntityType type;
    public GameObject spawned;
    public GameObject prefab;
    public bool autoSpawn = true;
    public bool linkToCamera = true;

	// Use this for initialization
	void Start () 
    {

	}
	
	// Update is called once per frame
	void Update () 
    {
        if (autoSpawn && spawned == null)
            ClearAndSpawn();
	}

    public void ClearAndSpawn()
    {
        if (spawned != null)
            Destroy(spawned);
        autoSpawn = false;
        spawned = Instantiate(prefab);
        spawned.transform.position = transform.position;
        GameController.instance.ballsController.AssignGeneratedEntity(spawned, type);
        GameObject.Find("Main Camera").GetComponent<GameCamera>().focusPoints.Add(spawned);
    }
}
