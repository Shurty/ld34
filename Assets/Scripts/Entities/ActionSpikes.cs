﻿using UnityEngine;
using System.Collections;

public class ActionSpikes : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D collider)
    {
        // It's a player - kill & reset
        if (collider.gameObject.GetComponentInChildren<BallsCollisionDetection>())
        {
            GameController.instance.OnPlayerDead(collider.GetComponentInParent<PlayerCharacter>());
        }
        // Manage monster cases ? 
        else
        {
        }
    }
}
