﻿using UnityEngine;
using System.Collections;

public class PlayerCharacter : MonoBehaviour 
{
    public EntityType type;
    public Blob blob;
    public Rigidbody2D body;

    public PlayerCharacter targetCharacter;
    public PlayerState state;

    public ParticleSystem spawnParticles;
    public ParticleSystem deathParticles;
    public float stateTimer = 0.0f;

	// Use this for initialization
	void Start () 
    {
        spawnParticles.Play();
        stateTimer = 0.5f;
        blob = GetComponent<Blob>();
        body = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        stateTimer -= Time.deltaTime;
        switch (state)
        {
            case PlayerState.SPAWNING:
                {
                    if (stateTimer <= 0.0f)
                    {
                        spawnParticles.Stop();
                        state = PlayerState.ALIVE;
                    }
                }
                break;
            case PlayerState.KILLED:
                {
                    if (stateTimer <= 0.0f)
                    {
                        deathParticles.Stop();
                        state = PlayerState.DEAD;
                    }
                }
                break;
            default:
                break;
        }
	}

    public void Lookat(PlayerCharacter other)
    {

    }

    public void KilledAnimation()
    {
        if (state == PlayerState.KILLED || state == PlayerState.DEAD)
            return;
        state = PlayerState.KILLED;
        GetComponent<Animator>().Play("Death");
        body.AddForce(-body.velocity * 2000.0f);
        stateTimer = 0.6f;
        deathParticles.Play();
    }
}
