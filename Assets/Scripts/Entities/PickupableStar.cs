﻿using UnityEngine;
using System.Collections;

public class PickupableStar : MonoBehaviour {

    public bool picked = false;

	// Use this for initialization
	void Start () {
        picked = false;
        GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1.0f);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (picked)
            return;

        if (collision.GetComponentInParent<PlayerCharacter>() == null)
            return;

        picked = true;
        GameController.instance.activeStarsGotten++;
        GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0.3f);
        GameController.instance.sounds.PlayStarPickupSound();
    }
}
