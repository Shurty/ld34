﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct RandomDecalType
{
    public float scale;
    public Sprite sprite;
}
