﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
[RequireComponent(typeof(RandomDecalLocation))]
public class RandomDecalLocationDebugRender : MonoBehaviour
{
    private RandomDecalLocation ent = null;
    private Camera camera;
    public Color color = new Color(0, 1, 0, 1);

	// Use this for initialization
	void Start () 
    {
        ent = gameObject.GetComponent<RandomDecalLocation>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        Vector3 pos = transform.position;
        if (ent == null)
        {
            ent = gameObject.GetComponent<RandomDecalLocation>();
        }
        if (ent == null)
        {
            camera = GameObject.Find("Main Camera").GetComponent<Camera>();
        }
 
        DebugUtils.DrawRect(
            pos + new Vector3(ent.rangeFrom.x, ent.rangeFrom.y, 0),
            pos + new Vector3(ent.rangeTo.x, ent.rangeFrom.y, 0),
            pos + new Vector3(ent.rangeTo.x, ent.rangeTo.y, 0),
            pos + new Vector3(ent.rangeFrom.x, ent.rangeTo.y, 0),
            color
            );
	}
}
