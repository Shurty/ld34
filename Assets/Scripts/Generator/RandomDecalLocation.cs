﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RandomDecalLocation : MonoBehaviour
{
    public GameObject decalPrefab;
    public RandomDecalType[] image;
    public List<GameObject> generated;
    public GameObject generationContainer;

    public bool generateUniques = false;
    public float minDistanceInGeneration = 0.0f;

    [Header("Gen. Distance")]
    public Vector2 rangeFrom;
    public Vector2 rangeTo;
    public int zLevel;
    public float scale = 1.0f;

    [Header("Chances")]
    public int minChance;
    public int maxChance;

	// Use this for initialization
	void Start () 
    {
        ClearAndGenerate();
	}

	// Update is called once per frame
	void Update () 
    {
	
	}

    public void ClearAndGenerate()
    {
        Clear();
        Generate();
    }

    public bool HasAlreadyBeenCreated(RandomDecalType dt)
    {
        foreach (GameObject obj in generated)
        {
            if (obj.GetComponentInChildren<SpriteRenderer>().sprite == dt.sprite)
            {
                return (true);
            }
        }
        return (false);
    }

    public RandomDecalType GetRandomDecal()
    {
        if (generateUniques)
        {
            List<RandomDecalType> decalsListed = new List<RandomDecalType>();

            foreach (RandomDecalType dt in image)
            {
                if (!HasAlreadyBeenCreated(dt))
                {
                    decalsListed.Add(dt);
                }
            }
            if (decalsListed.Count > 0)
                return (decalsListed[Random.Range(0, decalsListed.Count)]);
        }
        else
        {
            return (image[Random.Range(0, image.Length)]);
        }
        return (new RandomDecalType());
    }

    public bool IsPositionValid(Vector3 res)
    {
        foreach (GameObject obj in generated)
        {
            if (Vector3.Distance(obj.transform.position, res) <= minDistanceInGeneration)
                return (false);
        }
        return (true);
    }

    public Vector3 GetRandomPosition()
    {
        float maxGIt = 0;
        Vector3 res = transform.position;

        while (maxGIt < 10)
        {
            res = transform.position + new Vector3(
                Random.Range(rangeFrom.x, rangeTo.x),
                Random.Range(rangeFrom.y, rangeTo.y),
                zLevel);
            if (IsPositionValid(res))
                return (res);
            ++maxGIt;
        }
        return (res);
    }

    public void Generate()
    {
        int genCount = Random.Range(minChance, maxChance);

        while (genCount > 0)
        {
            GameObject newObj = Instantiate(decalPrefab);
            RandomDecalType decalType = GetRandomDecal();

            newObj.transform.SetParent(generationContainer.transform);
            newObj.transform.position = GetRandomPosition();
            newObj.transform.localScale = new Vector3(decalType.scale, decalType.scale, decalType.scale);
            newObj.GetComponentInChildren<SpriteRenderer>().sprite = decalType.sprite;
            generated.Add(newObj);
            --genCount;
        }
    }

    public void Clear()
    {
        foreach (GameObject obj in generated)
        {
            Destroy(obj);
        }
        generated.Clear();
    }
}
