﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIController : MonoBehaviour 
{
    [Header("UI Containers")]
    public GameObject gameHUD;
    public GameObject gamePause;

    public GameObject splashUI;
    public GameObject mainMenuUI;
    public GameObject creditsUI;
    public GameObject settingsUI;

    public GameObject levelSelectUI;
    public GameObject levelFinishedUI;
    public GameObject endGameUI;

    public GameObject fadingPanel;
    public GameObject cinematicUI;

    [Header("Inside UI Containers")]
    public GameObject levelsButtonsContainer;
    public GameObject starsContainer;
    public Text timeSpentContainer;

    public bool inPauseMenu = false;

    // Use this for initialization
	void Start () 
    {
	}
	
	// Update is called once per frame
	void Update () 
    {	

    }

    public void HideAll()
    {
        gamePause.SetActive(false);
        gameHUD.SetActive(false);
        splashUI.SetActive(false);
        mainMenuUI.SetActive(false);
        creditsUI.SetActive(false);
        settingsUI.SetActive(false);
        levelSelectUI.SetActive(false);
        endGameUI.SetActive(false);
        levelFinishedUI.SetActive(false);
        cinematicUI.SetActive(false);
    }

    public void StateSwitchToCinematic()
    {
        HideAll();
        cinematicUI.SetActive(true);
    }

    public void StateSwitchUpdate(GameState state)
    {
        HideAll();
        switch (state)
        {
            case GameState.SPLASH:
                splashUI.SetActive(true);
                break;
            case GameState.MAIN_MENU:
                mainMenuUI.SetActive(true);
                break;
            case GameState.CREDITS:
                creditsUI.SetActive(true);
                break;
            case GameState.SETTINGS:
                settingsUI.SetActive(true);
                break;
            case GameState.LEVEL_SELECT:
                levelSelectUI.SetActive(true);
                SetLevelsUI();
                break;
            case GameState.INGAME:
                gameHUD.SetActive(true);
                break;
            case GameState.ENDGAME:
                endGameUI.SetActive(true);
                break;
            case GameState.LEVEL_FINISHED:
                SetClearedUI();
                levelFinishedUI.SetActive(true);
                break;
            default:
                break;
        }
    }

    public void OnSplashStart()
    {
        GameController.instance.SetNextState(GameState.MAIN_MENU);
    }

    public void OnBackToMainMenu()
    {
        if (inPauseMenu)
        {
            HideAll();
            gamePause.SetActive(true);
            return;
        }
        GameController.instance.SetNextState(GameState.MAIN_MENU);
        GameController.instance.sounds.PlayClickSound();
    }

    public void OnForceBackToMainMenu()
    {
        inPauseMenu = false;
        GameController.instance.SetNextState(GameState.MAIN_MENU);
        GameController.instance.sounds.PlayClickSound();
    }

    public void OnMainMenuStart()
    {
        GameController.instance.SetNextState(GameState.LEVEL_SELECT);
        GameController.instance.sounds.PlayClickSound();
    }

    public void OnMainMenuSettings()
    {
        GameController.instance.SetNextState(GameState.SETTINGS);
        GameController.instance.sounds.PlayClickSound();
    }

    public void OnMainMenuCredits()
    {
        GameController.instance.SetNextState(GameState.CREDITS);
        GameController.instance.sounds.PlayClickSound();
    }

    public void OnMainMenuExit()
    {
        GameController.instance.Quit();
        GameController.instance.sounds.PlayClickSound();
    }

    public void OnGamePauseToggle()
    {
        if (inPauseMenu)
        {
            OnGamePauseOff();
        }
        else
        {
            OnGamePauseOn();
        }
    }

    public void OnGamePauseSettings()
    {
        HideAll();
        settingsUI.SetActive(true);
        GameController.instance.sounds.PlayClickSound();
    }

    public void OnGamePauseOff()
    {
        GameController.instance.Unpause();
        HideAll();
        gameHUD.SetActive(true);
        inPauseMenu = false;
        GameController.instance.sounds.PlayPauseOffSound();
    }

    public void OnGamePauseOn()
    {
        GameController.instance.Pause();
        HideAll();
        gamePause.SetActive(true);
        inPauseMenu = true;
        GameController.instance.sounds.PlayPauseOnSound();
    }

    public void OnSelectLevel(int id)
    {
        // Reindex to table coords
        GameController.instance.SetLevelById(id - 1);
        OnMainMenuStartLevel();
        GameController.instance.sounds.PlayClickSound();
    }

    public void OnCinematicSkip()
    {
        GameController.instance.cinematicPlayed = true;
        GameController.instance.playingCinematic = false;
        GameController.instance.StartSelectedLevel();
        GameController.instance.sounds.PlayClickSound();
    }

    public void OnMainMenuStartLevel()
    {
        GameController.instance.StartSelectedLevel();
        inPauseMenu = false;
        GameController.instance.sounds.PlayClickSound();
    }

    public IEnumerator FadeIn(float time)
    {
        fadingPanel.SetActive(true);
        fadingPanel.GetComponent<Image>().color = new Color(0, 0, 0, 0.0f);
        float actTime = 0.0f;
        float ratio = 0.0f;

        while (actTime < time)
        {
            actTime += Time.deltaTime;
            ratio = actTime / time;

            fadingPanel.GetComponent<Image>().color = new Color(0, 0, 0, ratio);
            yield return true;
        }
    }

    public IEnumerator FadeOut(float time)
    {
        fadingPanel.SetActive(true);
        fadingPanel.GetComponent<Image>().color = new Color(0, 0, 0, 1.0f);
        float actTime = 0.0f;
        float ratio = 0.0f;

        yield return new WaitForSeconds(0.1f);
        while (actTime < time)
        {
            actTime += Time.deltaTime;
            ratio = actTime / time;

            fadingPanel.GetComponent<Image>().color = new Color(0, 0, 0, 1.0f - ratio);
            yield return true;
        }
        fadingPanel.SetActive(false);
    }

    public void VolumeChanged(Slider item)
    {
        GameController.instance.volumeValue = item.value;
    }

    public void SFXChanged(Slider item)
    {
        GameController.instance.sfxValue = item.value;
    }

    public void InvertChanged(Toggle item)
    {
        GameController.instance.invertControls = item.isOn;
    }

    public void SetLevelsUI()
    {
        int id = 0;

        foreach (Button btn in levelsButtonsContainer.transform.GetComponentsInChildren<Button>())
        {
            if (id >= GameController.instance.levels.Length - 1)
            {
                btn.interactable = false;
                btn.GetComponentInChildren<Text>().text = "-";
            }
            else
            {
                GameLevel lvl = GameController.instance.levels[id];
                var rId = id + 1;

                btn.onClick.RemoveAllListeners();
                btn.onClick.AddListener(delegate { OnSelectLevel(rId); });
                btn.GetComponentInChildren<Text>().text = lvl.name;
                btn.interactable = true;
                if (lvl.locked)
                {
                    btn.interactable = false;
                }
            }
            ++id;
        }
    }

    public void SetClearedUI()
    {
        int cnt = 0;

        foreach (Image img in starsContainer.transform.GetComponentsInChildren<Image>())
        {
            if (cnt < GameController.instance.activeStarsGotten)
            {
                img.color = new Color(1, 1, 1, 1);
            }
            else
            {
                img.color = new Color(1, 1, 1, 0.2f);
            }
            cnt++;    
        }
        starsContainer.transform.GetComponentInChildren<Text>().text = GameController.instance.levelTimeSpent.ToString();
    }
}
