﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameCamera : MonoBehaviour {

    public List<GameObject> focusPoints;
    public Vector3 origin;

    [Header("Movement Ranges")]
    public Vector2 rangeMin;
    public Vector2 rangeMax;
    public float zoomMin; // Corresponds to the zoom range
    public float zoomMax; // Corresponds to the zoom range

    [Header("Movement Ranges")]
    public Vector3 positionOffset;

	// Use this for initialization
	void Start () 
    {
        MarkOrigin();
	}

    public void MarkOrigin()
    {
        origin = transform.position;
    }

    public Vector3 ClampPositionInRange(Vector3 targetOps)
    {
        return (new Vector3(
            Mathf.Clamp(targetOps.x, origin.x + rangeMin.x, origin.x + rangeMax.x),
            Mathf.Clamp(targetOps.y, origin.y + rangeMin.y, origin.y + rangeMax.y),
            origin.z
        ));
    }

    public float ClampZoomInRange(float dist)
    {
        return (Mathf.Clamp((dist - 3.0f), zoomMin, zoomMax));
    }

	// Update is called once per frame
	void Update () 
    {
	    if (focusPoints.Count > 0)
        {
            Vector3 targetPos = new Vector3();

            if (focusPoints.Count > 1)
            {
                float dist = Vector3.Distance(focusPoints[0].transform.position, focusPoints[1].transform.position);
                targetPos = (focusPoints[0].transform.position + focusPoints[1].transform.position) / 2.0f;
                GetComponent<Camera>().orthographicSize = ClampZoomInRange(dist);
            }
            else
            {
                targetPos = focusPoints[0].transform.position;
            }
            transform.position = ClampPositionInRange(targetPos + positionOffset);
        }
	}
}
