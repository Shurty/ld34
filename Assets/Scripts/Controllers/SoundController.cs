﻿using UnityEngine;
using System.Collections;

public class SoundController : MonoBehaviour {

    [Header("Global sources")]
    public AudioSource uiSource;
    public AudioSource musicSource;
    public AudioSource gameSource;

    [Header("Sounds")]
    public AudioClip clickSound;
    public AudioClip pauseOnSound;
    public AudioClip pauseOffSound;
    public AudioClip winSound;
    public AudioClip fusionSound;
    public AudioClip deathSoundPlayer1;
    public AudioClip deathSoundPlayer2;
    public AudioClip starPickupSound;
    public AudioClip interPressSound;

    [Header("Musics")]
    public AudioClip musicMenu;
    public AudioClip[] musicGame;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void PlayClickSound()
    {
        uiSource.clip = clickSound;
        uiSource.Play();
    }

    public void PlayPauseOnSound()
    {
        uiSource.clip = pauseOnSound;
        uiSource.Play();
    }

    public void PlayPauseOffSound()
    {
        uiSource.clip = pauseOffSound;
        uiSource.Play();
    }

    public void PlayWinLevelSound()
    {
        uiSource.clip = winSound;
        uiSource.Play();
    }

    public void PlayFusionSound()
    {
        gameSource.clip = fusionSound;
        gameSource.Play();
    }

    public void PlayPlayerOneDeathSound()
    {
        gameSource.clip = deathSoundPlayer1;
        gameSource.Play();
    }

    public void PlayPlayerTwoDeathSound()
    {
        gameSource.clip = deathSoundPlayer2;
        gameSource.Play();
    }

    public void PlayStarPickupSound()
    {
        gameSource.clip = starPickupSound;
        gameSource.Play();
    }

    public void PlayInterPressSound()
    {
        gameSource.clip = interPressSound;
        gameSource.Play();
    }

    public void PlayMenuMusic()
    {
        if (musicSource.clip == musicMenu)
            return;
        musicSource.clip = musicMenu;
        musicSource.Play();
    }

    public void PlayGameMusic()
    {
        if (GameController.instance.activeLevelId >= musicGame.Length)
            return;
        musicSource.clip = musicGame[GameController.instance.activeLevelId];
        musicSource.Play();
    }

    public void StopMusic()
    {
        musicSource.Stop();
    }
}
