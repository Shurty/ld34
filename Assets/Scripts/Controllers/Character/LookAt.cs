﻿using UnityEngine;
using System.Collections;

public class LookAt : MonoBehaviour {
    public Transform target;
    public float rotation = 90f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (target)
        {
            var dir = target.position - transform.position;
            var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg - rotation;
            transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        }
	}
}
