﻿using UnityEngine;
using System.Collections;

public class BallsCollisionDetection : MonoBehaviour {

    Animator animator;

	// Use this for initialization
	void Start () {
        // FIXME: This might not be very sexy
        animator = transform.parent.gameObject.GetComponent<Animator>();

	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        GameObject obj = other.gameObject;

        if (obj.GetComponent<BallsCollisionDetection>())
        {
            animator.SetTrigger("isKissing");
            GameController.instance.LevelCompleted();
        }
    }
}
