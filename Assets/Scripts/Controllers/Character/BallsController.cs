﻿using UnityEngine;
using System.Collections;

public class BallsController : MonoBehaviour 
{

    public Rigidbody2D ball1;
    public Rigidbody2D ball2;
    AudioSource ball1Sound;
    AudioSource ball2Sound;
    AudioSource bothBallsSound;

    Animator ball1Animator;
    Animator ball2Animator;

    public float moveForce = 3;
    public float topSpeed = 10;
    public float accelerationPerSec = 3;

    [Header("Active Forces")]
    public float currentForce;
    public float moveForceMax;

    [Header("Magnetization")]
    public float distanceRatio = 10.0f;
    public float ratioDivider = 4.0f;
    public float magnetMoveForceMax = 20000.0f;
    public float magnetRatio = 0.0f;

    public GameObject hitparticlesPrefab;

    enum State { Idle, Attract, Ball1Sing, Ball2Sing };

    State state = State.Idle;

    public void CreateVictoryParticles()
    {
        Vector3 center = (ball1.position + ball2.position) / 2.0f;
        GameObject obj = Instantiate(hitparticlesPrefab);

        obj.transform.position = center;
    }

    public void AssignGeneratedEntity(GameObject entity, EntityType type)
    {
        switch (type)
        {
            case EntityType.PLAYER_ONE:
                ball1 = entity.GetComponent<Rigidbody2D>();
                ball1Sound = ball1.GetComponent<AudioSource>();
                ball1Animator = ball1.GetComponentInChildren<Animator>();
                TryInitLookAts();
                break;
            case EntityType.PLAYER_TWO:
                ball2 = entity.GetComponent<Rigidbody2D>();
                ball2Sound = ball2.GetComponent<AudioSource>();
                ball2Animator = ball2.GetComponentInChildren<Animator>();
                TryInitLookAts();
                break;
            default:
                break;
        }
    }
    void TryInitLookAts()
    {
        if (ball2 && ball1)
        {
            Debug.Log("both balls");
            LookAt[] lookAts = ball2.GetComponentsInChildren<LookAt>();
            foreach (LookAt lookAt in lookAts)
            {
                Debug.Log("lookat 2");
                lookAt.target = ball1.transform;
            }
            lookAts = ball1.GetComponentsInChildren<LookAt>();
            foreach (LookAt lookAt in lookAts)
            {
                Debug.Log("lookat 1");

                lookAt.target = ball2.transform;
            }
        }
    }
	void Start ()
    {
        currentForce = moveForce;

        bothBallsSound = gameObject.GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void FixedUpdate () 
    {
        bool singBall1 = Input.GetButton("Ball1");
        bool singBall2 = Input.GetButton("Ball2");

        if (GameController.instance.invertControls)
        {
            bool tmp = singBall1;
            singBall1 = singBall2;
            singBall2 = tmp;
        }
        //Debug.Log("currentForce: " + currentForce);

        State newState;

        if (ball1 == null || ball2 == null)
            return;
        if (GameController.instance.gameEnded)
            return;

        if (singBall1 && singBall2)
        {
            newState = State.Attract;
            if (newState != state)
            {
                SetAudio(bothBallsSound);
                ball1Animator.SetBool("isSinging", true);
                ball2Animator.SetBool("isSinging", true);
                currentForce = moveForce;
            }
            //Debug.Log("attract");

            // attract
            magnetRatio = Vector3.Distance(ball1.transform.position, ball2.transform.position);
            magnetRatio = Mathf.Clamp(magnetRatio / distanceRatio, 0.0f, 1.0f);
            magnetRatio = Mathf.Min(1.0f, 1.0f / (magnetRatio * ratioDivider));
            IncreaseForceMagnet();

            if (ball2.velocity.magnitude < magnetMoveForceMax * magnetRatio || newState != state)
            {
                Vector3 force = (ball1.transform.position - ball2.transform.position).normalized * currentForce * Time.deltaTime;
                float yForceDiff = Mathf.Abs(force.y - force.y * magnetRatio);

                force.y *= magnetRatio;
                force.x += ((force.x > 0) ? yForceDiff : -yForceDiff);
                ball2.AddForce(force, ForceMode2D.Force);
            }
            if (ball1.velocity.magnitude < magnetMoveForceMax * magnetRatio || newState != state)
            {
                Vector3 force = (ball2.transform.position - ball1.transform.position).normalized * currentForce * Time.deltaTime;
                float yForceDiff = Mathf.Abs(force.y - force.y * magnetRatio);

                force.y *= magnetRatio;
                force.x += ((force.x > 0) ? yForceDiff : -yForceDiff);
                ball1.AddForce(force, ForceMode2D.Force);
            }
        }
        else if (singBall1)
        {
            newState = State.Ball1Sing;
            if (newState != state)
            {
                SetAudio(ball1Sound);
                ball1Animator.SetBool("isSinging", true);
                ball2Animator.SetBool("isSinging", false);
                currentForce = moveForce;
            }

            //Debug.Log("ball 1 sings");
            if (ball2.velocity.magnitude < topSpeed || newState != state)
            {
                IncreaseForce();
                Vector3 force = (ball2.transform.position - ball1.transform.position).normalized * currentForce * Time.deltaTime;
                float yForceDiff = Mathf.Abs(force.y - force.y * 0.3f);

                force.y *= 0.3f;
                force.x += ((force.x > 0) ? yForceDiff : -yForceDiff);
                ball2.AddForce(force, ForceMode2D.Force);
            }
        }
        else if (singBall2)
        {
            newState = State.Ball2Sing;
            if (newState != state)
            {
                SetAudio(ball2Sound);
                ball1Animator.SetBool("isSinging", false);
                ball2Animator.SetBool("isSinging", true);
                currentForce = moveForce;
            }

            //Debug.Log("ball 2 sings");
            if (ball1.velocity.magnitude < topSpeed || newState != state)
            {
                IncreaseForce();
                Vector3 force = (ball1.transform.position - ball2.transform.position).normalized * currentForce * Time.deltaTime;
                float yForceDiff = Mathf.Abs(force.y - force.y * 0.3f);

                force.y *= 0.3f;
                force.x += ((force.x > 0) ? yForceDiff : -yForceDiff);
                ball1.AddForce(force, ForceMode2D.Force);
            }
        }
        else
        {
            SetAudio(null);
            ball1Animator.SetBool("isSinging", false);
            ball2Animator.SetBool("isSinging", false);
            newState = State.Idle;
            currentForce = moveForce;
        }
        state = newState;
    }

    void IncreaseForce()
    {
        currentForce += Time.deltaTime * moveForce * accelerationPerSec;
        currentForce = Mathf.Min(currentForce, moveForceMax);
    }

    void IncreaseForceMagnet()
    {
        currentForce += Time.deltaTime * moveForce * accelerationPerSec;
        currentForce = Mathf.Min(currentForce, Mathf.Max(moveForceMax, magnetMoveForceMax * magnetRatio));
    }
    
    void SetAudio(AudioSource audio)
    {
        ball1Sound.Stop();
        ball2Sound.Stop();
        bothBallsSound.Stop();

        if (audio)
        {
            audio.Play();
        }
    }

    public void StopSounds()
    {
        bothBallsSound.Stop();
        ball1Sound.Stop();
        ball2Sound.Stop();
    }
}
