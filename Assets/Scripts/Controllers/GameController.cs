﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameController : MonoBehaviour 
{
    // Singleton Definition
    public static GameController instance;

    public BallsController ballsController;
    public UIController ui;
    public SoundController sounds;

    public GameLevel[] levels;
    public GameLevel activeLevel;
    public int activeLevelId;
    public bool gameEnded;
    public bool cinematicPlayed;
    public bool playingCinematic;
    public float timeBeforeEndingCinematic;

    [Header("Micro state machine")]
    public GameState currentState = GameState.NONE;
    public GameState nextState = GameState.NONE;
    public float transitionDuration;
    public float transitionDurationDef;

    public float volumeValue = 1.0f;
    public float sfxValue = 1.0f;
    public bool invertControls = false;
    public float levelTimeSpent = 0.0f;
    public int activeStarsGotten = 0;

    // Use this for initialization
	void Start () 
    {
        if (GameController.instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            GameController.instance = this;
            DontDestroyOnLoad(this.gameObject);
            SwitchStateNow(nextState);
            activeLevel = levels[activeLevelId];
            StartCoroutine(ui.FadeOut(0.2f));
        }
	}

    // Update is called once per frame
    void Update()
    {
        if (currentState == GameState.INGAME)
        {
            levelTimeSpent += Time.deltaTime;
        }
        if (currentState == GameState.SPLASH && nextState == GameState.NONE && Input.anyKeyDown)
        {
            SetNextState(GameState.MAIN_MENU);
        }
        if (nextState != GameState.NONE)
        {
            transitionDuration -= Time.deltaTime;

            if (transitionDuration <= 0.0f)
            {
                SwitchStateNow(nextState);
            }
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (currentState == GameState.INGAME)
            {
                ui.OnGamePauseToggle();
            }
        }
        if (playingCinematic)
        {
            timeBeforeEndingCinematic -= Time.deltaTime;
            if (timeBeforeEndingCinematic <= 0.0f)
            {
                cinematicPlayed = true;
                playingCinematic = false;
                SetNextState(GameState.INGAME);
            }
        }
    }

    public void SwitchStateNow(GameState state)
    {
        nextState = GameState.NONE;
        currentState = state;
        gameEnded = false;
        if (state == GameState.INGAME && activeLevel.hasCinematic && cinematicPlayed == false &&
            !playingCinematic)
        {
            cinematicPlayed = false;
            playingCinematic = true;
            timeBeforeEndingCinematic = activeLevel.cinematicTime;
            ui.StateSwitchToCinematic();
            StartCoroutine(ui.FadeOut(0.3f));
            SceneManager.LoadScene(activeLevel.cinematicName);
            Debug.Log("Game State: Changed to state #" + currentState);
            Debug.Log("Game State: Playing cinematic #" + activeLevel.cinematicName);
            return;
        }
        if (activeLevelId == levels.Length - 1 && currentState != GameState.ENDGAME)
        {
            activeLevelId = 0;
            SwitchStateNow(GameState.ENDGAME);
            return;
        }
        switch (state)
        {
            case GameState.SPLASH:
                SceneManager.LoadScene("SplashScene");
                sounds.PlayMenuMusic();
                break;
            case GameState.MAIN_MENU:
                SceneManager.LoadScene("MainMenu");
                sounds.PlayMenuMusic();
                break;
            case GameState.CREDITS:
                SceneManager.LoadScene("Credits");
                sounds.PlayMenuMusic();
                break;
            case GameState.SETTINGS:
                SceneManager.LoadScene("Settings");
                sounds.PlayMenuMusic();
                break;
            case GameState.LEVEL_SELECT:
                SceneManager.LoadScene("LevelSelect");
                sounds.PlayMenuMusic();
                break;
            case GameState.INGAME:
                SceneManager.LoadScene(activeLevel.sceneId);
                levelTimeSpent = 0.0f;
                activeStarsGotten = 0;
                sounds.PlayGameMusic();
                break;
            case GameState.ENDGAME:
                SceneManager.LoadScene("Endgame");
                sounds.PlayMenuMusic();
                break;
            case GameState.LEVEL_FINISHED:
                sounds.PlayWinLevelSound();
                sounds.StopMusic();
                break;
            default:
                break;
        }
        Debug.Log("Game State: Changed to state #" + currentState);
        ui.StateSwitchUpdate(currentState);
        StartCoroutine(ui.FadeOut(0.3f));
    }

    public void SetNextState(GameState newState)
    {
        if (nextState != GameState.NONE)
            return;
        Time.timeScale = 1.0f;
        transitionDuration = transitionDurationDef;
        nextState = newState;
        StartCoroutine(ui.FadeIn(0.5f));
    }

    public void LevelCompleted()
    {
        if (gameEnded)
            return;

        ballsController.ball1.isKinematic = true;
        ballsController.ball2.isKinematic = true;
        ballsController.CreateVictoryParticles();

        if (activeLevel.starsGotten < activeStarsGotten)
            activeLevel.starsGotten = activeStarsGotten;
        if (activeLevel.goldTime > levelTimeSpent)
            activeLevel.starsGotten = 3;
        else if (activeLevel.silverTime > levelTimeSpent && activeLevel.starsGotten != 3)
            activeLevel.starsGotten = 2;
        else if (activeLevel.bronzeTime > levelTimeSpent && activeLevel.starsGotten != 2)
            activeLevel.starsGotten = 1;

        Debug.Log("Level " + activeLevelId + " (" + activeLevel.name + ") complete!");
        Debug.Log("-- Made in " + levelTimeSpent + "s for " + activeLevel.starsGotten + " stars");
        //sounds.PlayFusionSound();
        sounds.PlayWinLevelSound();
        ballsController.StopSounds();
        if (levels.Length <= activeLevelId + 1)
        {
            SetNextState(GameState.ENDGAME);
        }
        else
        {
            activeLevel = levels[activeLevelId + 1];
            activeLevel.locked = false;
            activeLevelId++;
            //SwitchStateNow(GameState.LEVEL_FINISHED);
            currentState = GameState.LEVEL_FINISHED;
            ui.StateSwitchUpdate(currentState);
        }
        gameEnded = true;
    }

    public void ContinueToNextLevel()
    {
        cinematicPlayed = false;
        SetNextState(GameState.INGAME);
    }

    public void SetLevelById(int id)
    {
        cinematicPlayed = false;
        playingCinematic = false;
        activeLevelId = id;
        activeLevel = levels[id];
    }

    public IEnumerator DeadNextState()
    {
        yield return new WaitForSeconds(0.5f);
        SetNextState(GameState.INGAME);
    }

    public void OnPlayerDead(PlayerCharacter player)
    {
        if (gameEnded)
            return;
        Debug.Log("Player Died! Restart level");
        gameEnded = true;
        player.KilledAnimation();
        if (player.type == EntityType.PLAYER_ONE)
            sounds.PlayPlayerOneDeathSound();
        else
            sounds.PlayPlayerTwoDeathSound();
        StartCoroutine(DeadNextState());
    }

    public void StartSelectedLevel()
    {
        SetNextState(GameState.INGAME);
    }

    public void Unpause()
    {
        Time.timeScale = 1.0f;
    }

    public void Pause()
    {
        Time.timeScale = 0.0f;
    }

    public void Quit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
	    Application.Quit();
#endif
    }
}
