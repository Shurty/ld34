﻿using UnityEngine;
using System.Collections;

public class MeshFromVertices : Object {
        public static Mesh CreateForObject(Vector2[] vertices2D, GameObject obj)
        {
/*            // Create Vector2 vertices
            Vector2[] vertices2D = new Vector2[] {
                new Vector2(0,0),
                new Vector2(0,50),
                new Vector2(50,50),
                new Vector2(50,100),
                new Vector2(0,100),
                new Vector2(0,150),
                new Vector2(150,150),
                new Vector2(150,100),
                new Vector2(100,100),
                new Vector2(100,50),
                new Vector2(150,50),
                new Vector2(150,0),
            };*/
            Mesh mesh = new Mesh();

            UpdateMesh(mesh, vertices2D);

            // Set up game object with mesh;
            // we trust the object has a meshrenderer
            MeshFilter filter = obj.AddComponent(typeof(MeshFilter)) as MeshFilter;
            filter.mesh = mesh;
            return mesh;
    }

    public static void UpdateMesh(Mesh mesh, Vector2[] vertices2D)
    {
        // Use the triangulator to get indices for creating triangles
        
        Triangulator tr = new Triangulator(vertices2D);
        Vector3[] vertices = new Vector3[vertices2D.Length];
        for (int i = 0; i < vertices.Length; i++)
        {
            //Debug.Log("x: " + vertices2D[i].x + " ; y: " + vertices2D[i].y);
            vertices[i] = new Vector3(vertices2D[i].x, vertices2D[i].y, 0);
        }
        int[] indices = tr.Triangulate();
        mesh.vertices = vertices;
        mesh.triangles = indices;
        mesh.RecalculateNormals();
        mesh.RecalculateBounds();
    }

}
