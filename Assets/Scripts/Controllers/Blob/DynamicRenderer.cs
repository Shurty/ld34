﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MeshRenderer))]
public class DynamicRenderer : MonoBehaviour {
    public Transform[] points;
    
    Mesh mesh;

	// Use this for initialization
	void Start () {
        Vector2[] vertices = new Vector2[points.Length];
        for (int i = 0; i < vertices.Length; i++)
        {
            vertices[i] = new Vector2(points[i].localPosition.x * 1.1f, points[i].localPosition.y * 1.1f);// -new Vector2(transform.position.x, transform.position.y);
        }
        mesh = MeshFromVertices.CreateForObject(vertices, gameObject);
	}
	
	void FixedUpdate () {
        Vector2[] vertices = new Vector2[points.Length];
        for (int i = 0; i < vertices.Length; i++)
        {
            vertices[i] = new Vector2(points[i].localPosition.x * 1.1f, points[i].localPosition.y * 1.1f);// -new Vector2(transform.position.x, transform.position.y);
 //           vertices[i] = new Vector2(points[i].position.x, points[i].position.y);
        }
        MeshFromVertices.UpdateMesh(mesh, vertices);
	}
}
