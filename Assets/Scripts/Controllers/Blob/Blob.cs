﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class Blob : MonoBehaviour 
{
    [Header("Spring interactions")]
    public int nbSprings = 5;
    public float distanceSpring = 1.5f; // relative to scale 1
    public float frequencyForCenterSpring = 3f;
    public float frequencyBetweenPoints = 2f;

    [Header("Spring self parameters")]
    public float springMass = 0.6f;
    public float springDrag = 5.0f;
    public float cubeSize = 0.2f;
    public Vector2 sideGravity = new Vector2(0, -9);

    Rigidbody2D body2D;
    DynamicRenderer renderer;
    CircleCollider2D innerCollider;

	// Use this for initialization
	void Start () {

        body2D = GetComponent<Rigidbody2D>();
        innerCollider = gameObject.AddComponent<CircleCollider2D>();
        gameObject.layer = LayerMask.NameToLayer("BlobCenter");

        renderer = gameObject.AddComponent<DynamicRenderer>();
        renderer.points = new Transform[nbSprings];
        float angle = 0;
        float angleStep = Mathf.Deg2Rad * 360 / nbSprings;
        float distanceBetweenJoints = 0;
        Rigidbody2D lastJointBody = null;
        SpringJoint2D firstJointSpring = null;
        for (int i = 0; i < nbSprings;i++)
        {
            float x = distanceSpring * Mathf.Cos(angle) + transform.localPosition.x;
            float y = distanceSpring * Mathf.Sin(angle) + transform.localPosition.y;

            angle += angleStep;
            GameObject springObj = new GameObject();
            springObj.AddComponent<ConstantForce2D>().force = sideGravity;

            springObj.layer = LayerMask.NameToLayer("Blob");
            springObj.transform.localPosition = new Vector3(x, y, 0);
            springObj.transform.SetParent(gameObject.transform);
            SpringJoint2D jointSpring = springObj.AddComponent<SpringJoint2D>();
            jointSpring.frequency = frequencyBetweenPoints;

            BoxCollider2D boxSpring = springObj.AddComponent<BoxCollider2D>();
            boxSpring.size = new Vector2(cubeSize, cubeSize);
            Rigidbody2D bodySpring = springObj.GetComponent<Rigidbody2D>();
            bodySpring.mass = springMass;
            bodySpring.drag = springDrag;

            SpringJoint2D selfSpring = gameObject.AddComponent<SpringJoint2D>();
            selfSpring.connectedBody = bodySpring;
            selfSpring.frequency = frequencyForCenterSpring;
            selfSpring.dampingRatio = 0f;
            selfSpring.distance = distanceSpring;

            if (lastJointBody)
            {
                if (distanceBetweenJoints == 0)
                {
                    distanceBetweenJoints = Vector3.Distance(springObj.transform.position, lastJointBody.position);
                }
                jointSpring.distance = distanceBetweenJoints;
                //Debug.Log("lastJoint: " + lastJointBody);
                jointSpring.connectedBody = lastJointBody;
            }
            else
            {
                firstJointSpring = jointSpring;
            }
            renderer.points[i] = springObj.transform;
            lastJointBody = bodySpring;
        }
        innerCollider.radius = distanceSpring - distanceBetweenJoints - cubeSize;
        firstJointSpring.connectedBody = lastJointBody;
        firstJointSpring.distance = distanceBetweenJoints;
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
