﻿using UnityEngine;
using System.Collections;

public class FixedInitialAngleAndPosition : MonoBehaviour
{
    Vector3 angle;
    Vector3 offset;

    // Use this for initialization
    void Start()
    {
        offset = transform.position - transform.parent.position;
        angle = transform.eulerAngles;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = transform.parent.position + offset;
        transform.eulerAngles = angle;
    }
}
