﻿using UnityEngine;
using System.Collections;

public class FollowPoints : MonoBehaviour {
    public WayPoint nextWayPoint;
    public float speed = 3f;
    public Transform priorityTarget = null;

    public bool run = true;

    public bool requirePlayerOne = false;
    public bool requirePlayerTwo = false;

    public bool playerOnePresent = false;
    public bool playerTwoPresent = false;

    public float continueToNextDelay = 0.0f;
    
    // Use this for initialization
	void Start () {
        //Enable();
        if (nextWayPoint)
            continueToNextDelay = nextWayPoint.waitDelay;
	}
	
	// Update is called once per frame
	void Update () 
    {
        Vector3 target;

        if (!run || nextWayPoint == null)
            return;
        
        if (priorityTarget != null)
        {
            target = priorityTarget.transform.position;
        }
        else
        {
            if (!nextWayPoint)
            {
                return;
            }
            target = nextWayPoint.gameObject.transform.position;
        }
        float step = speed * Time.deltaTime;
        gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, target, step);
        if (priorityTarget == null && gameObject.transform.position == nextWayPoint.transform.position)
        {
            continueToNextDelay -= Time.deltaTime;
            if (continueToNextDelay <= 0.0f)
            {
                nextWayPoint = nextWayPoint.nextWayPoint;
                continueToNextDelay = nextWayPoint.waitDelay;
            }
        }
	}

    public void Disable()
    {
        this.enabled = false;
    }
    public void Enable()
    {
        this.enabled = true;
    }

    public void VerifyRunConditions()
    {
        if (run == false && (requirePlayerOne == true || requirePlayerTwo == true))
        {
            if ((playerOnePresent == false && requirePlayerOne == true) ||
                (playerTwoPresent == false && requirePlayerTwo == true))
                return;
            run = true;
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (!col.gameObject.transform.parent)
            return;
        GameObject obj = col.gameObject.transform.parent.gameObject;

        if (obj == GameController.instance.ballsController.ball1.gameObject)
        {
            playerOnePresent = true;
            VerifyRunConditions();
        }
        if (obj == GameController.instance.ballsController.ball2.gameObject)
        {
            playerTwoPresent = true;
            VerifyRunConditions();
        }
        if (obj.transform.parent == null)
            obj.transform.SetParent(this.transform);
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (!col.gameObject.transform.parent)
            return;
        GameObject obj = col.gameObject.transform.parent.gameObject;

        if (obj == GameController.instance.ballsController.ball1.gameObject)
        {
            playerOnePresent = false;
        }
        if (obj == GameController.instance.ballsController.ball2.gameObject)
        {
            playerTwoPresent = false;
        }
        if (obj.transform.parent == this.transform)
            obj.transform.SetParent(null);
    }

}
