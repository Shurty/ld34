﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
[RequireComponent(typeof(WayPoint))]
public class WayPointDebugRender : MonoBehaviour
{

    private WayPoint waypoint = null;
    private Camera camera;
    public Color color = new Color(1, 0, 0, 1);
	// Use this for initialization
	void Start () {
	    waypoint = gameObject.GetComponent<WayPoint>();
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 pos = transform.position;
        if (waypoint == null)
        {
            waypoint = gameObject.GetComponent<WayPoint>();
        }
        if (camera == null)
        {
            camera = GameObject.Find("Main Camera").GetComponent<Camera>();
        }

        if (waypoint.nextWayPoint != null)
        {
            Debug.DrawLine(waypoint.transform.position, waypoint.nextWayPoint.transform.position, color);
        }
 
        DebugUtils.DrawRect(
            pos + new Vector3(-0.5f, -0.5f, 0),
            pos + new Vector3(0.5f, -0.5f, 0),
            pos + new Vector3(0.5f, 0.5f, 0),
            pos + new Vector3(-0.5f, 0.5f, 0),
            color
            );
	}
}
