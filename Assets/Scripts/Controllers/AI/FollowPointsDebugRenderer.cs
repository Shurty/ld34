﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
[RequireComponent(typeof(FollowPoints))]
public class FollowPointsDebugRenderer : MonoBehaviour
{
    private FollowPoints followPoints = null;
    private Camera camera;
    public Color color = new Color(1, 0, 0, 1);

	// Use this for initialization
	void Start () {
        followPoints = gameObject.GetComponent<FollowPoints>();
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 pos = transform.position;
        if (followPoints == null)
        {
            followPoints = gameObject.GetComponent<FollowPoints>();
        }
        if (camera == null)
        {
            camera = GameObject.Find("Main Camera").GetComponent<Camera>();
        }

        if (followPoints.nextWayPoint != null)
        {
            Debug.DrawLine(followPoints.transform.position, followPoints.nextWayPoint.transform.position, color);
        }

        DebugUtils.DrawRect(
            pos + new Vector3(-0.5f, -0.5f, 0),
            pos + new Vector3(0.5f, -0.5f, 0),
            pos + new Vector3(0.5f, 0.5f, 0),
            pos + new Vector3(-0.5f, 0.5f, 0),
            color
            );
	}
}
