﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(FollowPoints), typeof(Collider2D))]
public class AIEnemy : MonoBehaviour {
    enum State { Patrol, Chase, Wait };

    State state = State.Patrol;
    FollowPoints followPoints;
    Collider2D characterTrigger;
    WayPoint firstWayPoint;

    float timerChase;
    public float timeToStopChase = 0.5f;

	// Use this for initialization
	void Start () {
	    followPoints = gameObject.GetComponent<FollowPoints>();
        firstWayPoint = followPoints.nextWayPoint;
        characterTrigger = gameObject.GetComponent<Collider2D>();
	}
	
	// Update is called once per frame
	void Update () {
        //Debug.Log("state: " + state);
        if (state == State.Patrol)
        {
            // nothing to do
        }
        else if (state == State.Chase)
        {
            //Debug.Log("timerChase: " + timerChase);
            // exhaust
            timerChase += Time.deltaTime;
            if (timerChase >= timeToStopChase)
            {
                followPoints.priorityTarget = null;
                ChangeState(State.Patrol);
                timerChase = 0;
            }
        }
	}

    void ChangeState(State newState)
    {
        // disable everything
        followPoints.Disable();
        // activate the right one
        if (newState == State.Patrol)
        {
            followPoints.Enable();
        }
        else if (newState == State.Chase)
        {
            followPoints.Enable();
        }
        state = newState;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        // chase player
        followPoints.priorityTarget = other.gameObject.transform;
        ChangeState(State.Chase);
        Debug.Log("triggerEntered");
    }
}
