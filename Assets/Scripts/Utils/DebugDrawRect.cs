﻿using UnityEngine;
using System.Collections;

public class DebugUtils
{
	public static void DrawRect(Vector3 a, Vector3 b, Vector3 c, Vector3 d, Color col)
    {
        Debug.DrawLine(a, b, col);
        Debug.DrawLine(b, c, col);
        Debug.DrawLine(c, d, col);
        Debug.DrawLine(d, a, col);
    }
}
