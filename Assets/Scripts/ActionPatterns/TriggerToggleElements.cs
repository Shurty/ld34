﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TriggerToggleElements : MonoBehaviour {

    public GameObject[] elementsToToggle;
    public List<ActionResponseType> triggerEventsRequired;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnActionEventTriggered(ActionResponseType resType)
    {
        foreach (ActionResponseType type in triggerEventsRequired)
        {
            if (type == resType)
            {
                triggerEventsRequired.Remove(type);
                break;
            }
        }
        if (triggerEventsRequired.Count == 0)
        {
            foreach (GameObject toggleTgt in elementsToToggle)
            {
                toggleTgt.SetActive(!toggleTgt.activeInHierarchy);
            }
        }
    }
}
