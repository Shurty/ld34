﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(FollowPoints))]
public class TriggerPlatform : MonoBehaviour
{
    public List<ActionResponseType> triggerEventsRequired;
    FollowPoints toggleTgt;

	// Use this for initialization
	void Start () {
        toggleTgt = GetComponent<FollowPoints>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    
    void OnActionEventTriggered(ActionResponseType resType)
    {
        foreach (ActionResponseType type in triggerEventsRequired)
        {
            if (type == resType)
            {
                triggerEventsRequired.Remove(type);
                break;
            }
        }
        if (triggerEventsRequired.Count == 0)
        {
            toggleTgt.run = true;
        }
    }
}
